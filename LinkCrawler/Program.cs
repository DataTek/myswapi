﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] urls = { "http://www.corelogic.com/", "http://www.homedepot.com/" };

            List<string> links = new List<string>();
            IEnumerable<string> targets = new List<string> { "linkedin", "facebook", "twitter" };
            
            foreach (var url in urls)
            {
                LinkCrawler crawler = new LinkCrawler(url);
                var l = crawler.FindLinks(url);
                //links.AddRange(l.Result);
                //links.AddRange(l.Result.Where(s => s.ToLower().Contains(targets)));
                links.AddRange(l.Result.Where(s => targets.Any(v => s.ToLower().Contains(v))));
            }

            foreach (var l in links)
            {
                Console.WriteLine(l);
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
