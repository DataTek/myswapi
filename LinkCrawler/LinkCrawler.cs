﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace LinkCrawler
{
    public class LinkCrawler
    {
        private readonly string urlBase;
        public LinkCrawler(string url)
        {
            urlBase = url;
        }

        WebClient GetClient()
        {
            //var client = new HttpClient();
            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");


            //client.BaseAddress = new Uri(urlBase);

            return client;
        }

        public async Task<IEnumerable<string>> FindLinks(string url)
        {
            string result = "";
            using (var client = GetClient())
            {
                //HttpResponseMessage response =  await client.GetAsync(url);
                //response.EnsureSuccessStatusCode();
                //result = await response.Content.ReadAsStringAsync();
                result = await client.DownloadStringTaskAsync(new Uri(url));
            }

            return LinkExtractor.Extract(result);
        }
    }
}