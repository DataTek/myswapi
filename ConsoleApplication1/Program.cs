﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //var match = Regex.Matches("How are youyou you you you you?", "you");
            var date = DateTime.Now;
            //DateTime.Today;
            //Console.WriteLine(CountSteps(100));
            ////for (int i = 0; i < 5; i++)
            ////{
            ////    Console.WriteLine(Fib(i));
            ////}
            //Console.WriteLine(CountFib(100));
            //Console.WriteLine(count(10));
            //fibn();
            linqTest();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static void linqTest()
        {
            string s = "this is a test string!";
            var d = s.GroupBy(c => c)
                .OrderBy(c => c.Key)
                .ToDictionary(grp => grp.Key, grp => grp.Count());
            foreach (var item in d)
            {
                Console.WriteLine("{0}: {1}", item.Key, item.Value);
            }

        }

        private static void fibn()
        {
            long prev = 0;
            for (int i = 5; i < 50; i++)
            {
                //Console.WriteLine("Fib({0}) : {1}", i, Fib(i));
                if (prev > 0)
                {
                    Console.WriteLine("Ratio: {0}", Fib(i) / (double)prev);
                }
                prev = Fib(i);
            }
        }

        public static Dictionary<string, Task<long>> results = new Dictionary<string, Task<long>>();
        private static long CountSteps(int steps)
        {
            //Console.WriteLine("step : {0}", steps);
            if (steps < 1) return 0;
            else if (steps < 3) return steps;
            else
            {
                string f = FindKey(steps - 2);
                string s = FindKey(steps - 1);
                if (!results.ContainsKey(f))
                {
                    results[f] = Task<long>.Factory.StartNew(() => CountSteps(steps - 2));
                }
                if (!results.ContainsKey(s))
                {
                    results[s] = Task<long>.Factory.StartNew(() => CountSteps(steps - 1));
                }
                //Task<int> first = Task<int>.Factory.StartNew(() => CountSteps(steps - 2));
                //Task<int> second = Task<int>.Factory.StartNew(() => CountSteps(steps - 1));
                //return first.Result + second.Result;
                return results[f].Result + results[s].Result;
            }
        }


        private static string FindKey(int steps)
        {
            return string.Format("CountSteps{0}", steps);
        }

        private static long count(int step)
        {
            long ret = 0;
            for (int i = step; i > 0; i = i - 1)
            {
                ret += count(i - 2) + 1;
            }
            return ret;
        }
        public static long Fib(int n)
        {
            long a = 0;
            long b = 1;
            
            for (int i = 0; i < n; i++)
            {
                long temp = a;
                a = b;
                b = temp + a;
            }
            return a;
        }
        public static long CountFib(int n)
        {
            return Fib(n) + Fib(n - 1);
        }
    }
}
