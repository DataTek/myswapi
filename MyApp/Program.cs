﻿using MyApp.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyApp.Model;
using System.Dynamic;

namespace MyApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!ValidateArgs(args))
            {
                Console.WriteLine("Argument error.");
                Console.WriteLine("Usage: MyApp.exe filmTitle property subproperty");
                Console.WriteLine("Eg: MyApp.exe \"Return of the Jedi\" starships name");
            }

            var client = new SwapiClient();
            var film = client.GetFilmAsync(args[0]).Result.results.FirstOrDefault();

            if (film != null)
            {
                FetchAndDisplay(film, args[1].ToLower(), args[2]);
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(); 
        }

        private static string Projection(object artifact, string name)
        {
            if (artifact == null)
            {
                return null;
            }
            var type = artifact.GetType();
            var p = type.GetProperty(name);
            return p.GetValue(artifact).ToString();
        }

        private static void FetchAndDisplay(Film film, string item, string name)
        {
            if (film != null)
            {                
                switch (item)
                {
                    case "characters":
                        var peoples = film.GetCharactersAsync().Result;
                        Display(peoples.Select(p => Projection(p, name)).Distinct());
                        break;
                    case "planets":
                        var planets = film.GetPlanetAsync().Result;
                        Display(planets.Select(p => Projection(p, name)).Distinct());
                        break;
                    case "starships":
                        var starships = film.GetStarshipAsync().Result;
                        Display(starships.Select(p => Projection(p, name)).Distinct());
                        break;
                }
            }
        }

        private static void Display(IEnumerable<string> items)
        {
            foreach(var item in items)
            {
                Console.WriteLine(item);
            }
        }

        private static bool ValidateArgs(string[] args)
        {
            var properties = new List<string>() { "characters", "planets", "starships" };
            if (args.Length == 3 && properties.Any(s => s == args[1].ToLower()))
            {
                return true;
            }

            return false;
        }
    }
}
