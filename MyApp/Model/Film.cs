﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp.Model
{
    public class Film : SwapiBase
    {
        public IEnumerable<string> characters { get; set; }
        public string director { get; set; }
        public string episode_id { get; set; }
        public IEnumerable<string> planets { get; set; }
        public string producer { get; set; }
        public IEnumerable<string> starships { get; set; }
        public string title { get; set; }
    }

 
    public class Films
    {
        public IEnumerable<Film> results { get; set; }
    }
}
