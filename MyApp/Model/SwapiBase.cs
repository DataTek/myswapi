﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp.Model
{
    public abstract class SwapiBase
    {
        public string url { get; set; }
        public string created { get; set; }
        public string edited { get; set; }
    }
}
