﻿using MyApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyApp.API
{
    public static class SwFilmExtension
    {
        async public static Task<IEnumerable<People>> GetCharactersAsync(this Film film)
        {
            SwapiClient client = new SwapiClient();
            return await client.GetListAsync<People>(film.characters);
        }

        async public static Task<IEnumerable<Planet>> GetPlanetAsync(this Film film)
        {
            SwapiClient client = new SwapiClient();
            return await client.GetListAsync<Planet>(film.planets);
        }

        async public static Task<IEnumerable<Starship>> GetStarshipAsync(this Film film)
        {
            SwapiClient client = new SwapiClient();
            return await client.GetListAsync<Starship>(film.starships);
        }
    }
}
