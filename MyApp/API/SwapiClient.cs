﻿using MyApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MyApp.API
{
    public class SwapiClient
    {
        protected readonly string UrlBase = @"http://swapi.co/api/";

        HttpClient GetClient()
        {
            var client = new HttpClient();

            client.BaseAddress = new Uri(UrlBase);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public async Task<T> GetAsync<T>(string url)
        {
            T result = default(T);

            using (HttpClient client = GetClient())
            {
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                result = await response.Content.ReadAsAsync<T>();
            }

            return result;
        }

        public async Task<IEnumerable<T>> GetListAsync<T>(IEnumerable<string> urls)
        {
            Task<IEnumerable<T>> t = Task.Run(() =>
            {
                List<T> items = new List<T>();
                foreach (var url in urls)
                {
                    T item = GetAsync<T>(url).Result;
                    items.Add(item);
                }
                return items.AsEnumerable();
            });

            return await t;
        }


        public async Task<Films> GetFilmAsync(string name)
        {
            string url = string.Format("films/?search={0}", name);
            return await GetAsync<Films>(url);
        }
    }
}
